﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Text;

namespace ClassLibrary
{
    public class motherboard
    {
        protected string name;
        protected string socet;
        protected double money;
        protected string chip;
        protected string brend;
        public string Brend
        {
            get
            {
                return brend;
            }
            set
            {
                brend = value;
            }
        }
        public string Name
        {
            get
            {
                return name;
            }
            set
            {
                name = value;
            }
        }
        public string Socet
        {
            get
            {
                return socet;
            }
            set
            {
                socet = value;
            }
        }
        public double Money
        {
            get
            {
                return money;
            }
            set
            {
                money = value;
            }
        }
        public string Chip
        {
            get
            {
                return chip;
            }
            set
            {
                chip = value;
            }
        }
        public motherboard()
        {

        }
        public motherboard(string Name, string Socet, double Money, string Chip, string Brend)
        {
            name = Name;
            socet = Socet;
            money = Money;
            chip = Chip;
            brend = Brend;
        }
    }
}
