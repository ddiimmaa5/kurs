﻿using System;
using System.Collections.Generic;
using System.Security;
using System.Text;

namespace ClassLibrary
{
    public class CP
    {
        protected string name;
        protected string company;
        protected double money;
        protected string socet;
        public string Name
        {
            get
            {
                return name;
            }

            set
            {
                name = value;
            }
        }
        public string Company
        {
            get
            {
                return company;
            }
            set
            {
                company = value;
            }
        }
        public double Money
        {
            get
            {
                return money;
            }
            set
            {
                money = value;
            }
        }
        public string Socet
        {
            get
            {
                return socet;
            }
            set
            {
                socet = value;
            }
        }
        public CP()
        {

        }
        public CP(string Name, string Company, int Money, string Socet)
        {
            name = Name;
            company = Company;
            money = Money;
            socet = Socet;
        }
    }
}
