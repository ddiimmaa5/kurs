﻿using System;

namespace ClassLibrary
{
    public class videogra
    {
        protected string name;
        protected string company;
        protected double money;
        public string Name
        {
            get
            {
                return name;
            }

            set
            {
                name = value;
            }
        }
        public string Company
        {
            get
            {
                return company;
            }
            set
            {
                company = value;
            }
        }
        public double Money
        {
            get
            {
                return money;
            }
            set
            {
                money = value;
            }
        }
        public videogra()
        {

        }
        public videogra(string Name, string Company, int Money)
        {
            name = Name;
            company = Company;
            money = Money;
        }
    }
}
